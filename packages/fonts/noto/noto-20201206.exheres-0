# Copyright 2015 Kylie McClain <somasis@exherbo.org>
# Copyright 2015 Wouter van Kesteren <woutershep@gmail.com>
# Distributed under the terms of the GNU General Public License v2
# Based in part on 'packages/fonts/google-roboto-fonts' in ::x11
#     Copyright 2015 Timo Gurr <tgurr@exherbo.org>

require github [ user=googlefonts pn=${PN}-fonts tag=v${PV}-phase3 ] \
    xfont

SUMMARY="Beautiful and free OpenType fonts for all languages"
DESCRIPTION="
When text is rendered by a computer, sometimes characters are displayed as 'tofu'. They are little
boxes to indicate your device doesn't have a font to display the text.

Google has been developing a font family called Noto, which aims to support all languages with a
harmonious look and feel. Noto is Google's answer to tofu. The name noto is to convey the idea
that Google's goal is to see 'no more tofu'. Noto has multiple styles and weights, and is freely
available to all.
"
HOMEPAGE+=" https://www.google.com/get/${PN}/"

LICENCES="OFL-1.1"
SLOT="0"
PLATFORMS="~amd64 ~armv8"
MYOPTIONS=""

DEPENDENCIES="
    suggestion:
        fonts/noto-cjk [[
            description = [ Noto Simplified and Traditional Chinese, Japanese, and Korean fonts ]
        ]]
        fonts/noto-emoji [[
            description = [ Noto Color Emoji font ]
        ]]
"

src_install() {
    insinto /usr/share/fonts/X11/${PN}
    doins hinted/ttf/*/Noto*.ttf
    doins unhinted/ttf/*/Noto*.ttf

    insinto /usr/share/fontconfig/conf.avail
    doins "${FILES}"/65-noto-mono.conf
    doins "${FILES}"/65-noto-sans.conf
    doins "${FILES}"/65-noto-serif.conf

    fix_fonts

    emagicdocs
}

