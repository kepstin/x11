# Copyright 2016-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure src_compile

SUMMARY="Qt Cross-platform application framework: Next generation UI controls"
DESCRIPTION="
The Qt Quick Controls 2 module delivers the next generation user interface
controls based on Qt Quick. In comparison to the desktop-oriented Qt Quick
Controls 1, Qt Quick Controls 2 are an order of magnitude simpler, lighter and
faster, and are primarily targeted towards embedded and mobile platforms.

Qt Quick Controls 2 are based on a flexible template system that enables rapid
development of entire custom styles and user experiences."

MYOPTIONS="examples"

QT_MIN_VER=$(ever range -3)

DEPENDENCIES="
    build+run:
        x11-libs/qtbase:${SLOT}[>=${QT_MIN_VER}][gui]
        x11-libs/qtdeclarative:${SLOT}[>=${QT_MIN_VER}]
        x11-libs/qtxmlpatterns:${SLOT}[>=${QT_MIN_VER}]
        examples? ( x11-libs/qtbase:5[sql] )
    run:
        x11-libs/qtgraphicaleffects:${SLOT}
    suggestion:
        kde-frameworks/qqc2-desktop-style:0 [[
            description = [ A style for QtQC2 which integrates natively with your desktop ]
        ]]
"

QQC2_MAJOR_VERSION=$(ever range 1-2)

qtquickcontrols2_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_src_configure
}

qtquickcontrols2_src_compile() {
    if has_version ${CATEGORY}/${PN} && \
        ! has_version ${CATEGORY}/${PN}[=${QQC2_MAJOR_VERSION}*] ; then
        # Ugly and probably fragile hack to avoid qmake being utterly stupid
        # and trying to link to the old, installed qtquickcontrols2.
        # The makefile isn't created at the start of this phase, so we need to
        # call qmake manually to create it and fix it below. To do this we
        # need to create the one for quicktemplates first, because
        # quickcontrols2 depends on it and to not get include paths from
        # the installed version.
        for dir in quicktemplates2 quickcontrols2 ; do
            edo pushd src/${dir}
            edo /usr/$(exhost --target)/lib/qt5/bin/qmake -o Makefile \
                QTDIR="/usr/$(exhost --target)/lib" \
                QMAKE="/usr/$(exhost --target)/lib/qt5/bin/qmake" \
                QMAKE_CFLAGS_RELEASE="${CFLAGS}" \
                QMAKE_CXXFLAGS_RELEASE="${CXXFLAGS}" \
                QMAKE_LFLAGS_RELEASE="${LDFLAGS}" \
                QMAKE_CFLAGS_DEBUG="${CFLAGS}" \
                QMAKE_CXXFLAGS_DEBUG="${CXXFLAGS}" \
                QMAKE_LFLAGS_DEBUG="${LDFLAGS}" \
                CONFIG+=nostrip
            edo popd
        done
        edo sed -e "s:\(= \$(SUBLIBS) \):\1-L${WORK}/lib -L/usr/$(exhost --target)/lib :" \
               -i src/quickcontrols2/Makefile
    fi

    qt_src_compile
}

