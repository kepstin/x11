# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure

SUMMARY="Qt Cross-platform application framework: QtSpeech"
DESCRIPTION="
The module enables a Qt application to support accessibility features such as
text-to-speech, which is useful for end-users who are visually challenged or
cannot access the application for whatever reason. The most common use case
where text-to-speech comes in handy is when the end-user is driving and cannot
attend the incoming messages on the phone. In such a scenario, the messaging
application can read out the incoming message."

MYOPTIONS="
    examples
    flite   [[ description = [ Build a plugin supporting Flite ] ]]
    speechd [[ description = [ Build a plugin supporting Speech Dispatcher ] ]]
"

QT_MIN_VER="$(ever range -3)"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        x11-libs/qtbase:${SLOT}[>=${QT_MIN_VER}]
        flite? (
            app-speech/flite
            sys-sound/alsa-lib
            x11-libs/qtmultimedia:${SLOT}[>=${QT_MIN_VER}]
        )
        speechd? ( app-speech/speechd )
"

qtspeech_src_configure() {
    if option examples ; then
        EQMAKE_PARAMS+=( QT_BUILD_PARTS+=examples )
    else
        EQMAKE_PARAMS+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_params=(
        $(qt_enable flite)
        $(qt_enable flite flite-alsa)
        $(qt_enable speechd)
    )

    eqmake -- "${qmake_params[@]}"
}

