# Copyright 2011-2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require cogl
require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 ] ]

LICENCES="LGPL-2.1"
SLOT="1.0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    gtk-doc
    gobject-introspection
    gstreamer [[ description = [ enable gstreamer support ] ]]
    (
        sdl
        sdl2
    ) [[
        number-selected = at-most-one
        description = [ enable the SDL backend ]
    ]]
    wayland
    X [[ presumed = true ]]
    ( linguas:
        an ar as ast be bg bs ca ca@valencia cs da de el en_CA en_GB eo es eu fa fr fur gl he hi hr
        hu id it ja km kn ko lt lv ml nb ne nl oc or pa pl pt pt_BR ro ru sk sl sr sr@latin sv ta te
        th tr ug uk vi zh_CN zh_HK zh_TW
    )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.13] )
    build+run:
        dev-libs/libglvnd[X=]
        x11-libs/cairo[>=1.10]
        dev-libs/glib:2[>=2.28.0]
        x11-libs/pango[>=1.20][gobject-introspection?]
        x11-libs/gdk-pixbuf:2.0[>=2.0]
        x11-dri/mesa[wayland?][X?]
        x11-dri/libdrm
        gobject-introspection? ( gnome-desktop/gobject-introspection:1[>=0.9.5] )
        gstreamer? (
            media-libs/gstreamer:1.0[gobject-introspection?]
            media-plugins/gst-plugins-base:1.0
        )
        sdl? ( media-libs/SDL:0 )
        sdl2? ( media-libs/SDL:2 )
        wayland? ( sys-libs/wayland[>=1.1.90] )
        X? (
            x11-libs/libX11
            x11-libs/libXext
            x11-libs/libXfixes[>=3]
            x11-libs/libXdamage
            x11-libs/libXcomposite[>=0.4]
            x11-libs/libXrandr[>=1.2]
        )

        !x11-libs/clutter:1[=1.6*] [[
            description = [ Cogl used to be part of Clutter, thus we collide with old versions ]
            resolution = uninstall-blocked-after
        ]]
"

# conform tests fail
RESTRICT="test"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    '--disable-standalone'
    '--disable-debug'
    '--enable-cairo'
    '--disable-profile'
    '--enable-glib'
    '--enable-cogl-pango'
    '--enable-cogl-path'
    '--enable-gdk-pixbuf'
    '--disable-examples-install'
    '--disable-gles1'
    '--enable-gles2'
    '--disable-null-egl-platform'
    '--disable-gdl-egl-platform'
    '--enable-kms-egl-platform'
    '--disable-android-egl-platform'
    '--disable-emscripten'
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'gstreamer cogl-gst'
    'gobject-introspection introspection'
    'gtk-doc'
    'sdl'
    'sdl2'
    'wayland wayland-egl-platform'
    'wayland wayland-egl-server'
    'X gl'
    'X glx'
    'X xlib-egl-platform'
)

src_install() {
    default
    nonfatal edo rmdir "${IMAGE}/usr/bin"
}

