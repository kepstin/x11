# Copyright 2014 Jorge Aparicio
# Copyright 2014-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require qt qmake [ slot=5 ]

export_exlib_phases src_configure

SUMMARY="Wayland plugin for Qt"
HOMEPAGE="http://qt-project.org/wiki/${PN}"

LICENCES+="
    GPL-3
    HPND [[ note = [ wayland-protocol, wayland-txt-input-unstable ] ]]
    MIT  [[ note = [ wayland-{ivi-extension-protocol,xdg-shell-protocol} ] ]]
"
MYOPTIONS="
    examples
    vulkan
"

QT_MIN_VER="$(ever range -3)"

DEPENDENCIES="
    build:
        virtual/pkg-config
        vulkan? ( sys-libs/vulkan-headers )
    build+run:
        dev-libs/glib:2
        sys-libs/wayland[>=1.8.0]
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/libxkbcommon[>=0.2.0]
        x11-libs/qtbase:${SLOT}[>=${QT_MIN_VER}][gui]
        x11-libs/qtdeclarative:${SLOT}[>=${QT_MIN_VER}]
"

qtwayland_src_configure() {
    local qmake_parts=() qmake_enables=()

    if option examples ; then
        qmake_parts+=( QT_BUILD_PARTS+=examples )
    else
        qmake_parts+=( QT_BUILD_PARTS-=examples )
    fi

    qmake_enables+=(
        $(qt_enable vulkan feature-wayland-vulkan-server-buffer)
    )

    eqmake "${qmake_parts[@]}" -- "${qmake_enables[@]}"
}

